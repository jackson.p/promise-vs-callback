// uma promise é um intermediador para uma ação assincrona, ele é um objeto capaz de
// receber uma função que pode tratar um erro ocorrido na ação assim como uma função
// para trabalhar com os dados devolvidos na resposta de uma ação bem sucedida.

const fs = require('fs')

const readyFile = fileName => new Promise((resolve, reject) =>{
  fs.readFile(fileName, (err, contents) => {
    if(err){
      reject()
    } else {
      resolve(contents)
    }
  })
})

const resolve = content => console.log(String(content))

const reject = () => console.log('deu ruim')

readyFile('./example.txt').then(resolve, reject)

readyFile('./example_not_exist.txt').then(resolve, reject)


// - 'ha mas então é a mesma coisa que uma callback'

// ele é sim similar a uma callback mas com o diferencial organizacional, uma promise 
// ao ser instanciada imediatamente retorna um objeto que representa a promessa de que
// algo vai acontecer, seja um erro ou um sucesso, junto a isso temos métodos capazes de 
// detectar se ela foi resolvida (then) ou rejeitada (catch).
// ambas as funções retornam uma nova promise, e é ai que mora a sua vantagem em cima de 
// simples callbacks, pois podemos retornar: nada, um simples valor ou UMA NOVA PROMISE que 
// irá 'substituir' a que por padrão é retornada no then/catch e assim poderemos encadear 
// mais métodos then e catch fazendo com que o código cresça para baixo e não mais para 
// frente, tornando sua manutenção bem mais simples

// quando não passamos nada como parâmetro do resolve da primeira promise retornada no then
readyFile('example.txt')
  .then(content => {
    console.log('first file: ', String(content))
  })
  .then(content => {
    // undefined
    console.log(content)
  })
  
// quando passamos um valor como parâmetro do resolve da primeira promise retornada no then
readyFile('example.txt')
  .then(content => {
    // este valor vai ser o argumento do resolve da promise gerada por esse then
    return 'paçoquinha'
  })
  .then(content => {
    // paçoquinha
    console.log(content)
  })

// quando passamos uma nova promise como parâmetro do resolve da primeira promise retornada 
// no then
readyFile('example.txt')
  .then(content => {
    console.log('first file: ', String(content))
    return readyFile('example_2.txt')
  })
  .then(content => {
    console.log('second file: ', String(content))
  })
