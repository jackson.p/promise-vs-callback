// callbacks são funções chamadas para atender a resposta de uma ação assincrona
// normalmente ela possui dois parâmetros, o primeiro é o erro, ela é preenchida
// caso a ação tenha falhado, e a segunda é a resposta informando que a ação foi
// efetuada.

const fs = require('fs')

const callback = (error, response) => {
  if (error){
    console.log('deu erro')
  } else {
    console.log(String(response))
  }
}

fs.readFile('./example.txt', callback)

fs.readFile('./example_not_exist.txt', callback)